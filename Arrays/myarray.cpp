#include "stdafx.h"
#include <time.h>
#include "myarray.h"

void Create(Array& object)
{
	if (object.array != NULL)
		delete[](object.array);
	cout << "������� ��� �������" << endl;
	string name;
	cin >> name;
	object.name = name;
	cout << "������� ������ �������" << endl;
	int size;
	cin >> size;
	object.size = size;
	object.array = new double[size];
	cout << "1.��������� ������ �������." << endl;
	cout << "2.��������� ������ ���������� ����������" << endl;
	int choice = 0;
	while (choice != 1 && choice != 2)
	{
		cin >> choice;
		if (choice != 1 && choice != 2)
			cout << "�������� ����� ����!" << endl;
	};
	if (choice == 1)
	{
		for (int i = 0; i < object.size; i++)
		{
			cout << "������� " << i + 1 << " ������� �������" << endl;
			cin >> object.array[i];
		}
	}
	else
	{
		srand(time(NULL));
		for (int i = 0; i < object.size; i++)
			object.array[i] = rand();
	}
}

bool Compare(Array& FirstArray, Array& SecondArray)
{
	if (FirstArray.size != SecondArray.size)
		return false;
	for (int i = 0; i < FirstArray.size; i++)
	{
		if (FirstArray.array[i] != SecondArray.array[i])
			return false;
	}
	return true;
}


int FindLine(Array& object)
{
	int count = 1;
	int tmp = 1;
	for (int i = 1; i <= object.size; i++)
	{
		if (object.array[i] == object.array[i - 1])
		{
			tmp++;
			if (tmp > count)
				count = tmp;
		}
		else
			tmp = 1;
	}
	return count;
}

void TopThree(Array& object, double top[3])
{
	if (object.size < 3)
		return;
	top[0] = top[1] = top[2] = 0;
	top[0] = object.array[0];
	for (int i = 1; i < object.size; i++)
	{
		if (object.array[i] > top[0])
			top[0] = object.array[i];
	}
	for (int i = 1; i < 3; i++)
	{
		double latestmax = 0;
		for (int j = 0; j < object.size; j++)
		{
			
			if (object.array[j] == top[i-1] && latestmax == top[i-1])
			{
				top[i] = object.array[j];
				continue;
			}
			if (object.array[j] < top[i - 1] && object.array[j] > top[i])
				top[i] = object.array[j];
			if (object.array[j] > latestmax)
				latestmax = object.array[j];
		}
	}
}

int FindMin(Array& object)
{
	double min = object.array[0];
	int index = 0;
	for (int i = 1; i < object.size; i++)
	{
		if (object.array[i] < min)
		{
			min = object.array[i];
			index = i;
		}
	}
	return index;
}

int FindElement(Array& object, double Element)
{
	for (int i = 0; i < object.size; i++)
	{
		if (object.array[i] == Element)
			return i;
	}
	return -1;
}

void SortArray(Array& object, int l, int r)
{
	int i = l;
	int j = r;
	int middle = object.array[(i + j) / 2];
	do
	{
		while (middle > object.array[i])
			i++;
		while (middle < object.array[j])
			j--;
		if (i <= j)
		{
			swap(object.array[i], object.array[j]);
			i++;
			j--;
		}
	} while (i < j);
	if (i < r) SortArray(object, i, r);
	if (j > l) SortArray(object, l, j);

}

double Sum(Array &object)
{
	double sum = 0;
	for (int i = 0; i < object.size; i++)
	{
		sum += object.array[i];
	}
	return sum;
}

double Difference(Array &object)
{
	double Max = object.array[0];
	for (int i = 1; i < object.size; i++)
	{
		if (object.array[i] > Max)
			Max = object.array[i];
	}
	return (Max - object.array[FindMin(object)]);
}