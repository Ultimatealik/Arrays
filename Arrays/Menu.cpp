#include "stdafx.h"
#include "Menu.h"

void ShowMenu()
{
	cout << "1.������� ������" << endl;
	cout << "2.����� ������ ������������ �������� �������" << endl;
	cout << "3.����� ������� �������" << endl;
	cout << "4.������������� ������" << endl;
	cout << "5.����� ����� ������� ������������������ ���������� ��������� �������" << endl;
	cout << "6.����� ��� ���������� �������� �������" << endl;
	cout << "7.�������� ��� �������" << endl;
	cout << "8.������� ������ �� �����" << endl;
	cout << "9.������� ����� ���� ��������� �������" << endl;
	cout << "10. ������� ������� ����� ���������� � ���������� ����������" << endl;
	cout << "0.�����" << endl;
}

Array &ChooseArray(Array& FirstArray, Array& SecondArray)
{
	if (FirstArray.array == NULL || SecondArray.array == NULL)
	{
		if (FirstArray.array == NULL)
			return SecondArray;
		else if (SecondArray.array == NULL)
			return FirstArray;
	}
	cout << "�������� ������ ��� ���������" << endl;
	cout << "1." << FirstArray.name << endl;
	cout << "2." << SecondArray.name << endl;
	int choice = 0;
	while (choice != 1 && choice != 2)
	{
		cin >> choice;
	};
	if (choice == 1)
		return FirstArray;
	else
		return SecondArray;
}



void Onscreen(Array& object)
{
	cout << object.name << ": ";
	for (int i = 0; i < object.size; i++)
	{
		cout << object.array[i] << " ";
	}
	cout << endl;
	system("pause");
}

void Menu(Array& FirstArray, Array& SecondArray)
{
	enum menu { quiet, create, findmin, findElement, sorting, findLine, findBiggest, match, onscreen, sum, differ };
	int item = -1;
	Create(FirstArray);
	while (item != quiet)
	{
		system("cls");
		ShowMenu();
		cin >> item;
		switch (item)
		{
		case create:
		{
			system("cls");
			if (FirstArray.array == NULL || SecondArray.array == NULL)
			{
				if (FirstArray.array == NULL)
					Create(FirstArray);
				else if (SecondArray.array == NULL)
					Create(SecondArray);
				break;
			}
			Create(ChooseArray(FirstArray, SecondArray));
			break;
		}
		case findmin:
		{
			system("cls");
			int min = 0;
			min = FindMin(ChooseArray(FirstArray, SecondArray));
			cout << "������ ������������ �������� �������: " << min << endl;
			system("pause");
			break;
		}
		case findElement:
		{
			system("cls");
			double Element = 0;
			int index = 0;
			cout << "������� ������� ��������: ";
			cin >> Element;
			index = FindElement(ChooseArray(FirstArray, SecondArray), Element);
			if (index < 0)
			{
				cout << "������� �������� ��� � �������" << endl;
			}
			else
			{
				cout << "������ �������� ��������: " << index << endl;
			}
			system("pause");
			break;
		}
		case sorting:
		{
			system("cls");
			Array& TempArray = ChooseArray(FirstArray, SecondArray);
			SortArray(TempArray, 0, TempArray.size - 1);
			cout << "������ ������������" << endl;
			system("pause");
			break;
		}
		case findLine:
		{
			system("cls");
			int line = 0;
			cout << "���������� ������������������ ���������� ��������� ������� ������� �� " << FindLine(ChooseArray(FirstArray, SecondArray)) << " ���������" << endl;
			system("pause");
			break;
		}
		case findBiggest:
		{
			system("cls");
			double top[3] = { 0, 0, 0 };
			TopThree(ChooseArray(FirstArray, SecondArray), top);
			cout << "���������� �������� � �������: ";
			for (int i = 0; i < 3; i++)
			{
				cout << top[i] << " ";
			}
			cout << endl;
			system("pause");
			break;
		}
		case match:
		{
			system("cls");
			if (SecondArray.size == 0)
			{
				cout << "��� ��������� ���������� ��� �������!" << endl;
				system("pause");
				break;
			}
			if (Compare(FirstArray, SecondArray))
			{
				cout << "������� �����" << endl;
			}
			else
			{
				cout << "������� �� �����" << endl;
			}
			system("pause");
			break;
		}
		case onscreen:
		{
			system("cls");
			Onscreen(ChooseArray(FirstArray, SecondArray));
			break;
		}
		case sum:
		{
			system("cls");
			double summ = 0;
			summ = Sum(ChooseArray(FirstArray, SecondArray));
			cout << "����� ���� ���������: " << summ << endl;
			system("pause");
			break;
		}
		case differ:
		{
			system("cls");
			double dif = 0;
			dif = Difference(ChooseArray(FirstArray, SecondArray));
			cout << "������� ����� ���������� � ���������� ����������: " << dif << endl;
			system("pause");
			break;
		}
		}

	}
}