#pragma once
#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

struct Array
{
	string name;
	double* array = NULL;
	int size = 0;
};

void Create(Array& object);

bool Compare(Array& FirstArray, Array& SecondArray);

int FindLine(Array& object);

int FindMin(Array& object);

void SortArray(Array& object, int l, int r);


int FindElement(Array& object, double Element);

void TopThree(Array& object, double top[3]);

double Sum(Array &object);

double Difference(Array &object);