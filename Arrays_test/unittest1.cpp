#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Arrays/myarray.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Arrays_test
{		
	TEST_CLASS(UnitTest_Compare)
	{
	public:
		
		TEST_METHOD(Compare_True)
		{
			Array TestArray1; Array TestArray2;
			TestArray1.size = TestArray2.size = 5;
			TestArray1.array = new double[TestArray1.size];
			TestArray2.array = new double[TestArray2.size];
			TestArray1.array[0] = TestArray2.array[0] = 1.0;
			TestArray1.array[1] = TestArray2.array[1] = 2.5;
			TestArray1.array[2] = TestArray2.array[2] = 3.3;
			TestArray1.array[3] = TestArray2.array[3] = 4.6;
			TestArray1.array[4] = TestArray2.array[4] = 5.1;
			Assert::AreEqual(true, Compare(TestArray1, TestArray2));
		}

		TEST_METHOD(Compare_False)
		{
			Array TestArray1; Array TestArray2;
			TestArray1.size = TestArray2.size = 2;
			TestArray1.array = new double[TestArray1.size];
			TestArray2.array = new double[TestArray2.size];
			TestArray1.array[0] = TestArray2.array[0] = 1.0;
			TestArray1.array[1] = 3.0;
			TestArray2.array[1] = 1.5;
			if (Compare(TestArray1, TestArray2))
				Assert::Fail();
		}

		TEST_METHOD(Compare_WrongSize)
		{
			Array TestArray1; Array TestArray2;
			TestArray1.size = 3;
			TestArray2.size = 5;
			TestArray1.array = new double[TestArray1.size];
			TestArray2.array = new double[TestArray2.size];
			TestArray1.array[0] = TestArray2.array[0] = 1.0;
			TestArray1.array[1] = TestArray2.array[1] = 2.5;
			TestArray1.array[2] = TestArray2.array[2] = 3.3;
			TestArray2.array[3] = 4.6;
			TestArray2.array[4] = 5.1;
			if (Compare(TestArray1, TestArray2))
				Assert::Fail();
		}
	};


	TEST_CLASS(UnitTest_FindLine)
	{
		TEST_METHOD(FindLine_Right)
		{
			Array TestArray;
			TestArray.size = 6;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 0;
			TestArray.array[1] = 3;
			TestArray.array[2] = 3;
			TestArray.array[3] = 3;
			TestArray.array[4] = 2;
			Assert::AreEqual(3, FindLine(TestArray));
		}
	};

	TEST_CLASS(UnitTest_TopThree)
	{
		TEST_METHOD(TopThree_Right)
		{
			Array TestArray;
			TestArray.size = 6;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 0;
			TestArray.array[1] = 7;
			TestArray.array[2] = 3;
			TestArray.array[3] = 2;
			TestArray.array[4] = 5;
			double testtop1[3] = { 7, 5, 3 };
			double top[3];
			TopThree(TestArray, top);
			int tmp = 0;
			for (int i = 0; i < 3; i++)
			{
				if (testtop1[i] != top[i])
					tmp++;
			}
			Assert::AreEqual(0, tmp);
		}

		TEST_METHOD(TopThree_Wrong)
		{
			Array TestArray;
			TestArray.size = 6;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 8;
			TestArray.array[1] = 7;
			TestArray.array[2] = 3;
			TestArray.array[3] = 2;
			TestArray.array[4] = 5;
			double testtop1[3] = { 7, 5, 3 };
			double top[3];
			TopThree(TestArray, top);
			int tmp = 0;
			for (int i = 0; i < 3; i++)
			{
				if (testtop1[i] != top[i])
					tmp++;
			}
			if (tmp == 0)
				Assert::Fail;
		}

		TEST_METHOD(TopThree_Equal)
		{
			Array TestArray;
			TestArray.size = 6;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 7;
			TestArray.array[2] = 7;
			TestArray.array[3] = 2;
			TestArray.array[4] = 5;
			double testtop1[3] = { 7, 7, 7 };
			double top[3];
			TopThree(TestArray, top);
			int tmp = 0;
			for (int i = 0; i < 3; i++)
			{
				if (testtop1[i] != top[i])
					tmp++;
			}
			Assert::AreEqual(0, tmp);
		}
	};

	TEST_CLASS(UnitTest_FindMin)
	{
		TEST_METHOD(FindMin_Right)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 0;
			TestArray.array[2] = 3;
			Assert::AreEqual(1, FindMin(TestArray));
		}
	};

	TEST_CLASS(UnitTest_FindElelemt)
	{
		TEST_METHOD(FindElement_Right)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 0;
			TestArray.array[2] = 3;
			Assert::AreEqual(2, FindElement(TestArray, 3.0));
		}

		TEST_METHOD(FindElement_NotExist)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 0;
			TestArray.array[2] = 3;
			Assert::AreEqual(-1, FindElement(TestArray, 10.0));
		}
	};

	TEST_CLASS(UnitTest_SortArray)
	{
		TEST_METHOD(SortArray_Right)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 0;
			TestArray.array[2] = 3;
			Array TestArray2;
			TestArray2.size = 3;
			TestArray2.array = new double[TestArray2.size];
			TestArray2.array[0] = 0;
			TestArray2.array[1] = 3;
			TestArray2.array[2] = 7;
			SortArray(TestArray, 0, TestArray.size -1);
			Assert::AreEqual(true, Compare(TestArray, TestArray2));
		}
	};

	TEST_CLASS(UnitTest_Sum)
	{
		TEST_METHOD(Sum_Right)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 15;
			TestArray.array[2] = 24;
			Assert::AreEqual(46.0, Sum(TestArray));
		}

	};

	TEST_CLASS(UnitTest_Difference)
	{
		TEST_METHOD(Difference_Right)
		{
			Array TestArray;
			TestArray.size = 3;
			TestArray.array = new double[TestArray.size];
			TestArray.array[0] = 7;
			TestArray.array[1] = 15;
			TestArray.array[2] = 24;
			Assert::AreEqual(17.0, Difference(TestArray));
		}
	};
}